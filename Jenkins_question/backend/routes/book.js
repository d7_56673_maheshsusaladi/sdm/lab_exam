const { request, response } = require('express')
const express=require('express')

const db=require('../db')

const utils=require('../utils')


const router=express.Router()

router.get('/getbook',(request,response)=>{
    
    const {book_title}=request.body
    const connection=db.openConnection()

    const sql=`select * from Book where book_title='${book_title}'`

    connection.query(sql,(error,data)=>{
        connection.end()
          
        response.send(utils.createResult(error,data))

    })
})

router.post('/add',(request,response)=>{
    const{book_id,book_title,publisher_name,author_name}=request.body
    const connection=db.openConnection()

    const sql=`insert into Book (book_id,book_title,publisher_name,author_name)
    VALUES('${book_id}','${book_title}','${publisher_name}','${author_name}')`

    connection.query(sql,(error,data)=>{
        connection.end()
          
        response.send(utils.createResult(error,data))

    })

})
router.post('/edit/:id',(request,response)=>{
    const{id} =request.params
    const{publisher_name,author_name}=request.body
    const connection=db.openConnection()

    const sql=`update Book set publisher_name='${publisher_name}',
    author_name='${author_name}' 
    where book_id='${id}'`

    connection.query(sql,(error,data)=>{
        connection.end()
          
        response.send(utils.createResult(error,data))

    })

})
router.delete('/delete/:id',(request,response)=>{

    const{id} =request.params
    const connection=db.openConnection()

    const sql=`delete from Book
    where book_id='${id}'`

    connection.query(sql,(error,data)=>{
        connection.end()
          
        response.send(utils.createResult(error,data))

    })
})





module.exports=router